/**
 * Created by Arkady on 21.08.2015.
 */

angular.module('projectsApp', ['ngResource'])
    .controller('ProjectsController', ['$scope', '$http', '$resource',
        function ($scope, $http, $resource) {
            var Project = $resource('/projects');

            getProjects();

            function getProjects() {
                Project.query(function (data) {
                    $scope.projects = data;
                    getCompanies();
                }, function (error) {
                    showError(error.data);
                });
            }

            function getCompanies() {
                $http.get('/companies')
                    .success(function (data) {
                        $scope.companies = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            }

            $scope.add = function () {
                var project = new Project;

                project.name = $scope.name;
                project.status = $scope.status;
                project.company = $scope.company;

                Project.save(project, function () {
                    $scope.name = "";
                    $scope.status = "";
                    $scope.company = "";

                    getProjects();
                });
            };

            $scope.update = function (project) {
                $http.put('/projects/' + project.id, project)
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.delete = function (id) {
                $http.delete('/projects/' + id)
                    .success(function (data) {
                        getProjects(data);
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.find = function () {
                $http.get('/projects/' + $scope.projectName)
                    .success(function (data) {
                        $scope.project = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.findEmployeeProjects = function () {
                $http.get('/employees/' + $scope.employeeEmail + '/projects')
                    .success(function (data) {
                        $scope.projects = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.findCompanyProjects = function () {
                $http.get('/companies/' + $scope.companyName + '/projects')
                    .success(function (data) {
                        $scope.projects = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };
        }
    ]);