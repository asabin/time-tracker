/**
 * Created by Arkady on 27.10.2015.
 */

angular.module('projectApp', ['ngResource'])
    .controller('ProjectController', ['$scope', '$http', '$routeParams',
        function ($scope, $http, $routeParams) {

            findById();

            function findById() {
                $http.get('/project/' + $routeParams.id)
                    .success(function (data) {
                        $scope.project = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            }

            $scope.save = function (project) {
                $http.put('/projects/' + project.id, project)
                    .error(function (data) {
                        showError(data);
                    });
            };
        }
    ]);