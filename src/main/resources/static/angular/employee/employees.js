/**
 * Created by Arkady on 21.08.2015.
 */

angular.module('employeesApp', ['ngResource'])
    .controller('EmployeesController', ['$scope', '$http', '$resource',
        function ($scope, $http, $resource) {
            var Employee = $resource('/employees');

            getEmployees();

            function getEmployees() {
                Employee.query(function (data) {
                    $scope.employees = data;
                }, function (error) {
                    showError(error.data);
                });
            }

            $scope.add = function () {
                var employee = new Employee;

                employee.name = $scope.name;
                employee.surname = $scope.surname;
                employee.password = $scope.password;
                employee.email = $scope.email;
                employee.role = $scope.role;

                Employee.save(employee, function () {
                    $scope.name = "";
                    $scope.surname = "";
                    $scope.password = "";
                    $scope.email = "";
                    $scope.role = "";

                    getEmployees();
                });
            };

            $scope.update = function (employee) {
                $http.put('/employees/' + employee.id, employee)
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.delete = function (id) {
                $http.delete('/employees/' + id)
                    .success(function (data) {
                        getEmployees(data);
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.find = function () {
                $http.get('/employees/' + $scope.employeeEmail)
                    .success(function (data) {
                        $scope.employee = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.findCompanyEmployees = function () {
                $http.get('/companies/' + $scope.companyName + '/employees')
                    .success(function (data) {
                        $scope.employees = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.findProjectEmployees = function () {
                $http.get('/projects/' + $scope.projectName + '/employees')
                    .success(function (data) {
                        $scope.employees = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };
        }
    ]);