/**
 * Created by Arkady on 26.10.2015.
 */

angular.module('employeeApp', ['ngResource'])
    .controller('EmployeeController', ['$scope', '$http', '$routeParams',
        function ($scope, $http, $routeParams) {

            var employeeId = $routeParams.id;

            findById();

            function findById() {
                $http.get('/employee/' + employeeId)
                    .success(function (data) {
                        $scope.employee = data;
                        $scope.employeeCompanies = data.companies;
                        $scope.employeeProjects = data.projects;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            }

            $scope.save = function (employee) {
                $http.put('/employees/' + employee.id, employee)
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.deleteCompany = function (companyId) {
                $http.delete('/employee/' + employeeId + '/companies/' + companyId)
                    .success(function () {
                        findById();
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.deleteProject = function (projectId) {
                $http.delete('/employee/' + employeeId + '/projects/' + projectId)
                    .success(function () {
                        findById();
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.addCompany = function () {
                $http.get('/employee/' + employeeId + '/companies')
                    .success(function (data) {
                        $scope.companies = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.saveCompany = function (company) {
                $http.put('/employee/' + employeeId + '/company', company)
                    .success(function () {
                        findById();
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.addProject = function () {
                $http.get('/employee/' + employeeId + '/projects')
                    .success(function (data) {
                        $scope.projects = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.saveProject = function (project) {
                $http.put('/employee/' + employeeId + '/project', project)
                    .success(function () {
                        findById();
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };
        }
    ]);