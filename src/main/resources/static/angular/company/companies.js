/**
 * Created by Arkady on 21.08.2015.
 */

angular.module('companiesApp', ['ngResource'])
    .controller('CompaniesController', ['$scope', '$http', '$resource',
        function ($scope, $http, $resource) {
            var Company = $resource('/companies');

            getCompanies();

            function getCompanies() {
                Company.query(function (data) {
                    $scope.companies = data;
                }, function (error) {
                    showError(error.data);
                });
            }

            $scope.add = function () {
                var company = new Company;

                company.name = $scope.name;
                company.status = $scope.status;

                Company.save(company, function () {
                    $scope.name = "";
                    $scope.status = "";

                    getCompanies();
                });
            };

            $scope.update = function (company) {
                $http.put('/companies/' + company.id, company)
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.delete = function (id) {
                $http.delete('/companies/' + id)
                    .success(function (data) {
                        getCompanies(data);
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.find = function () {
                $http.get('/companies/' + $scope.companyName)
                    .success(function (data) {
                        $scope.company = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.findEmployeeCompanies = function () {
                $http.get('/employees/' + $scope.employeeEmail + '/companies')
                    .success(function (data) {
                        $scope.companies = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };

            $scope.findProjectCompany = function () {
                $http.get('/projects/' + $scope.projectName + '/company')
                    .success(function (data) {
                        $scope.company = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            };
        }
    ]);