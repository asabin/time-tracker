/**
 * Created by Arkady on 27.10.2015.
 */

angular.module('companyApp', ['ngResource'])
    .controller('CompanyController', ['$scope', '$http', '$routeParams',
        function ($scope, $http, $routeParams) {

            findById();

            function findById() {
                $http.get('/company/' + $routeParams.id)
                    .success(function (data) {
                        $scope.company = data;
                    })
                    .error(function (data) {
                        showError(data);
                    });
            }

            $scope.save = function (company) {
                $http.put('/companies/' + company.id, company)
                    .error(function (data) {
                        showError(data);
                    });
            };
        }
    ]);