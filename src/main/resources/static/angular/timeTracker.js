/**
 * Created by Arkady on 07.10.2015.
 */

angular.module('timeTrackerApp', ['ngRoute',
    'employeesApp',
    'employeeApp',
    'companiesApp',
    'companyApp',
    'projectsApp',
    'projectApp'
])

    .config(['$routeProvider', '$locationProvider',
        function ($routeProvider, $locationProvider) {
            $locationProvider.html5Mode(true);

            $routeProvider
                .when('/employees', {
                    templateUrl: 'angular/employee/employees.html',
                    controller: 'EmployeesController'
                })
                .when('/employee/:id', {
                    templateUrl: 'angular/employee/employee.html',
                    controller: 'EmployeeController'
                })
                .when('/companies', {
                    templateUrl: 'angular/company/companies.html',
                    controller: 'CompaniesController'
                })
                .when('/company/:id', {
                    templateUrl: 'angular/company/company.html',
                    controller: 'CompanyController'
                })
                .when('/projects', {
                    templateUrl: 'angular/project/projects.html',
                    controller: 'ProjectsController'
                })
                .when('/project/:id', {
                    templateUrl: 'angular/project/project.html',
                    controller: 'ProjectController'
                })
                .otherwise('');
        }
    ]);

function showError(data) {
    alert("status: " + data.status +
        "\nerror: " + data.error +
        "\nexception: " + data.exception +
        "\nmessage: " + data.message);
}