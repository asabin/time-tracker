package org.arkady.timetracker.repository;

import org.arkady.timetracker.entity.Expense;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;

/**
 * Created by Arkady on 22.08.2015.
 */

public interface ExpenseRepository extends JpaRepository<Expense, Integer> {

    Expense findByTaskIdAndDate(int taskId, LocalDate date);
}