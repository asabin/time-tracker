package org.arkady.timetracker.repository;

import org.arkady.timetracker.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Arkady on 21.08.2015.
 */

public interface TaskRepository extends JpaRepository<Task, Integer> {

    Task findByName(String name);
}