package org.arkady.timetracker.repository;

import org.arkady.timetracker.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Arkady on 21.08.2015.
 */

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    Employee findByEmail(String email);
}