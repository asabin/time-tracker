package org.arkady.timetracker.repository;

import org.arkady.timetracker.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Arkady on 21.08.2015.
 */

public interface ProjectRepository extends JpaRepository<Project, Integer> {

    Project findByName(String name);
}