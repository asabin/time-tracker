package org.arkady.timetracker.repository;

import org.arkady.timetracker.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Arkady on 21.08.2015.
 */

public interface CompanyRepository extends JpaRepository<Company, Integer> {

    Company findByName(String name);
}