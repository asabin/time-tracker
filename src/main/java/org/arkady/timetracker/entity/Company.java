package org.arkady.timetracker.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by Arkady on 21.08.2015.
 */

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
public class Company {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int id;

    private String name;
    private boolean status;

    @JsonIgnore
    private LocalDate created;

    @JsonIgnore
    @ManyToMany(mappedBy = "companies")
    private List<Employee> employees = new ArrayList<>();

    @JsonBackReference
    @OneToMany(mappedBy = "company")
    private List<Project> projects = new ArrayList<>();

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
}