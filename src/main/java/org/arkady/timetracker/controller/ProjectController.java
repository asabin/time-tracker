package org.arkady.timetracker.controller;

import org.arkady.timetracker.entity.Company;
import org.arkady.timetracker.entity.Employee;
import org.arkady.timetracker.entity.Project;
import org.arkady.timetracker.service.CompanyService;
import org.arkady.timetracker.service.EmployeeService;
import org.arkady.timetracker.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arkady on 21.08.2015.
 */

@RestController
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private CompanyService companyService;

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    private List<Project> getProjects() {
        return projectService.findAll();
    }

    @RequestMapping(value = "/project/{id}", method = RequestMethod.GET)
    private Project getProject(@PathVariable int id) {
        return projectService.findById(id);
    }

    @RequestMapping(value = "/projects/{name}", method = RequestMethod.GET)
    private Project getProject(@PathVariable String name) {
        return projectService.findByName(name);
    }

    @RequestMapping(value = "/projects", method = RequestMethod.POST)
    private void addProject(@RequestBody Project project) {
        project.setStarted(LocalDate.now());
        project.setStatus(true);
        projectService.save(project);
    }

    @RequestMapping(value = "/projects/{id}", method = RequestMethod.PUT)
    private void updateProject(@RequestBody Project project, @PathVariable int id) {
        Project savedProject = projectService.findById(id);

        if (savedProject != null) {
            savedProject.setName(project.getName());
            savedProject.setStatus(project.isStatus());

            projectService.save(savedProject);
        }
    }

    @RequestMapping(value = "/projects/{id}", method = RequestMethod.DELETE)
    private void deleteProjectById(@PathVariable int id) {
        projectService.delete(id);
    }

    @RequestMapping(value = "employees/{employeeEmail}/projects", method = RequestMethod.GET)
    private List<Project> getEmployeeProjects(@PathVariable String employeeEmail) {
        Employee employee = employeeService.findByEmail(employeeEmail);

        if (employee != null)
            return employee.getProjects();

        return new ArrayList<>();
    }

    @RequestMapping(value = "companies/{companyName}/projects", method = RequestMethod.GET)
    private List<Project> getCompanyProjects(@PathVariable String companyName) {
        Company company = companyService.findByName(companyName);

        if (company != null)
            return company.getProjects();

        return new ArrayList<>();
    }
}