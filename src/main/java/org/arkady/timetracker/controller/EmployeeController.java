package org.arkady.timetracker.controller;

import org.arkady.timetracker.entity.Company;
import org.arkady.timetracker.entity.Employee;
import org.arkady.timetracker.entity.Project;
import org.arkady.timetracker.service.CompanyService;
import org.arkady.timetracker.service.EmployeeService;
import org.arkady.timetracker.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arkady on 21.08.2015.
 */

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/employees", method = RequestMethod.GET)
    private List<Employee> getEmployees() {
        return employeeService.findAll();
    }

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.GET)
    private Employee getEmployee(@PathVariable int id) {
        return employeeService.findById(id);
    }

    @RequestMapping(value = "/employees/{email}", method = RequestMethod.GET)
    private Employee getEmployee(@PathVariable String email) {
        return employeeService.findByEmail(email);
    }

    @RequestMapping(value = "/employees", method = RequestMethod.POST)
    private void addEmployee(@RequestBody Employee employee) {
        employee.setCreated(LocalDate.now());
        employeeService.save(employee);
    }

    @RequestMapping(value = "/employees/{id}", method = RequestMethod.PUT)
    private void updateEmployee(@RequestBody Employee employee, @PathVariable int id) {
        Employee savedEmployee = employeeService.findById(id);

        if (savedEmployee != null) {
            savedEmployee.setName(employee.getName());
            savedEmployee.setSurname(employee.getSurname());
            savedEmployee.setPassword(employee.getPassword());
            savedEmployee.setEmail(employee.getEmail());
            savedEmployee.setRole(employee.getRole());

            employeeService.save(savedEmployee);
        }
    }

    @RequestMapping(value = "/employees/{id}", method = RequestMethod.DELETE)
    private void deleteEmployeeById(@PathVariable int id) {
        employeeService.delete(id);
    }

    @RequestMapping(value = "companies/{companyName}/employees", method = RequestMethod.GET)
    private List<Employee> getCompanyEmployees(@PathVariable String companyName) {
        Company company = companyService.findByName(companyName);

        if (company != null)
            return company.getEmployees();

        return new ArrayList<>();
    }

    @RequestMapping(value = "projects/{projectName}/employees", method = RequestMethod.GET)
    private List<Employee> getProjectEmployees(@PathVariable String projectName) {
        Project project = projectService.findByName(projectName);

        if (project != null)
            return project.getEmployees();

        return new ArrayList<>();
    }

    @RequestMapping(value = "employee/{id}/companies", method = RequestMethod.GET)
    private List<Company> getCompanies(@PathVariable int id) {
        Employee employee = employeeService.findById(id);
        List<Company> employeeCompanies = employee.getCompanies();

        List<Company> allCompanies = companyService.findAll();

        if (employeeCompanies != null && !employeeCompanies.isEmpty()) {
            List<Company> companies = new ArrayList<>();

            allCompanies.stream()
                    .filter(company -> employeeCompanies.stream()
                            .allMatch(employeeCompany -> employeeCompany.getId() != company.getId()))
                    .forEach(companies::add);
            return companies;
        } else {
            return allCompanies;
        }
    }

    @RequestMapping(value = "employee/{id}/projects", method = RequestMethod.GET)
    private List<Project> getEmployeeProjects(@PathVariable int id) {
        Employee employee = employeeService.findById(id);
        List<Company> companies = employee.getCompanies();

        if (companies != null && !companies.isEmpty()) {
            ArrayList<Project> companiesProjects = new ArrayList<>();
            companies.forEach(company -> companiesProjects.addAll(company.getProjects()));

            List<Project> employeeProjects = employee.getProjects();

            if (employeeProjects != null && !employeeProjects.isEmpty()) {
                ArrayList<Project> projects = new ArrayList<>();

                companiesProjects.stream()
                        .filter(project -> employeeProjects.stream()
                                .allMatch(employeeProject -> employeeProject.getId() != project.getId()))
                        .forEach(projects::add);

                return projects;
            } else {
                return companiesProjects;
            }
        }

        return new ArrayList<>();
    }

    @RequestMapping(value = "employee/{employeeId}/companies/{companyId}", method = RequestMethod.DELETE)
    private void deleteCompany(@PathVariable int employeeId, @PathVariable int companyId) {
        Employee employee = employeeService.findById(employeeId);

        employee.getCompanies().removeIf(company -> company.getId() == companyId);

        List<Project> companyProjects = companyService.findById(companyId).getProjects();
        employee.getProjects().removeIf(project -> companyProjects.stream()
                .anyMatch(companyProject -> companyProject.getId() == project.getId()));

        employeeService.save(employee);
    }

    @RequestMapping(value = "employee/{employeeId}/projects/{projectId}", method = RequestMethod.DELETE)
    private void deleteProject(@PathVariable int employeeId, @PathVariable int projectId) {
        Employee employee = employeeService.findById(employeeId);

        employee.getProjects().removeIf(project -> project.getId() == projectId);

        employeeService.save(employee);
    }

    @RequestMapping(value = "employee/{employeeId}/company", method = RequestMethod.PUT)
    private void addCompany(@PathVariable int employeeId, @RequestBody Company company) {
        Employee employee = employeeService.findById(employeeId);

        if (company != null) {
            employee.getCompanies().add(company);
            employeeService.save(employee);
        }
    }

    @RequestMapping(value = "employee/{employeeId}/project", method = RequestMethod.PUT)
    private void addProject(@PathVariable int employeeId, @RequestBody Project project) {
        Employee employee = employeeService.findById(employeeId);

        if (project != null) {
            employee.getProjects().add(project);
            employeeService.save(employee);
        }
    }
}