package org.arkady.timetracker.controller;

import org.arkady.timetracker.entity.Company;
import org.arkady.timetracker.entity.Employee;
import org.arkady.timetracker.entity.Project;
import org.arkady.timetracker.service.CompanyService;
import org.arkady.timetracker.service.EmployeeService;
import org.arkady.timetracker.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arkady on 21.08.2015.
 */

@RestController
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/companies", method = RequestMethod.GET)
    private List<Company> getCompanies() {
        return companyService.findAll();
    }

    @RequestMapping(value = "/company/{id}", method = RequestMethod.GET)
    private Company getCompany(@PathVariable int id) {
        return companyService.findById(id);
    }

    @RequestMapping(value = "/companies/{name}", method = RequestMethod.GET)
    private Company getCompany(@PathVariable String name) {
        return companyService.findByName(name);
    }

    @RequestMapping(value = "/companies", method = RequestMethod.POST)
    private void addCompany(@RequestBody Company company) {
        company.setCreated(LocalDate.now());
        company.setStatus(true);
        companyService.save(company);
    }

    @RequestMapping(value = "/companies/{id}", method = RequestMethod.PUT)
    private void updateCompany(@RequestBody Company company, @PathVariable int id) {
        Company savedCompany = companyService.findById(id);

        if (savedCompany != null) {
            savedCompany.setName(company.getName());
            savedCompany.setStatus(company.isStatus());

            companyService.save(savedCompany);
        }
    }

    @RequestMapping(value = "/companies/{id}", method = RequestMethod.DELETE)
    private void deleteCompanyById(@PathVariable int id) {
        companyService.delete(id);
    }

    @RequestMapping(value = "employees/{employeeEmail}/companies", method = RequestMethod.GET)
    private List<Company> getEmployeeCompanies(@PathVariable String employeeEmail) {
        Employee employee = employeeService.findByEmail(employeeEmail);

        if (employee != null)
            return employee.getCompanies();

        return new ArrayList<>();
    }

    @RequestMapping(value = "projects/{projectName}/company", method = RequestMethod.GET)
    private Company getProjectCompany(@PathVariable String projectName) {
        Project project = projectService.findByName(projectName);

        if (project != null)
            return project.getCompany();

        return new Company();
    }
}