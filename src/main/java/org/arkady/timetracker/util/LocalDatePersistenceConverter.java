package org.arkady.timetracker.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Date;
import java.time.LocalDate;

/**
 * Created by Arkady on 24.08.2015.
 */

@Converter(autoApply = true)
public class LocalDatePersistenceConverter implements AttributeConverter<LocalDate, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDate entityValue) {
        return entityValue != null ? Date.valueOf(entityValue) : null;
    }

    @Override
    public LocalDate convertToEntityAttribute(Date databaseValue) {
        return databaseValue != null ? databaseValue.toLocalDate() : null;
    }
}