package org.arkady.timetracker.service.impl;

import org.arkady.timetracker.entity.Project;
import org.arkady.timetracker.repository.ProjectRepository;
import org.arkady.timetracker.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arkady on 21.08.2015.
 */

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Project save(Project project) {
        return projectRepository.saveAndFlush(project);
    }

    @Override
    public Project findById(Integer id) {
        return projectRepository.findOne(id);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        projectRepository.delete(id);
    }

    @Override
    public boolean exist(Integer id) {
        return projectRepository.exists(id);
    }

    @Override
    public Project findByName(String name) {
        return projectRepository.findByName(name);
    }
}