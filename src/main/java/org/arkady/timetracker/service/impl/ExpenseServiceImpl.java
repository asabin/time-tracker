package org.arkady.timetracker.service.impl;

import org.arkady.timetracker.entity.Expense;
import org.arkady.timetracker.repository.ExpenseRepository;
import org.arkady.timetracker.service.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by Arkady on 22.08.2015.
 */

@Service
public class ExpenseServiceImpl implements ExpenseService {

    @Autowired
    private ExpenseRepository expenseRepository;

    @Override
    public Expense save(Expense expense) {
        return expenseRepository.saveAndFlush(expense);
    }

    @Override
    public Expense findById(int id) {
        return expenseRepository.findOne(id);
    }

    @Override
    public List<Expense> findAll() {
        return expenseRepository.findAll();
    }

    @Override
    public void delete(int taskId) {
        expenseRepository.delete(taskId);
    }

    @Override
    public boolean exist(Integer id) {
        return expenseRepository.exists(id);
    }

    @Override
    public Expense findByTaskIdAndDate(int taskId, LocalDate date) {
        return expenseRepository.findByTaskIdAndDate(taskId, date);
    }
}