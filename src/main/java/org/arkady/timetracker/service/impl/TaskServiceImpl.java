package org.arkady.timetracker.service.impl;

import org.arkady.timetracker.entity.Task;
import org.arkady.timetracker.repository.TaskRepository;
import org.arkady.timetracker.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arkady on 21.08.2015.
 */

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    public Task save(Task task) {
        return taskRepository.saveAndFlush(task);
    }

    @Override
    public Task findById(Integer id) {
        return taskRepository.findOne(id);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        taskRepository.delete(id);
    }

    @Override
    public boolean exist(Integer id) {
        return taskRepository.exists(id);
    }

    @Override
    public Task findByName(String name) {
        return taskRepository.findByName(name);
    }
}