package org.arkady.timetracker.service.impl;

import org.arkady.timetracker.entity.Company;
import org.arkady.timetracker.repository.CompanyRepository;
import org.arkady.timetracker.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Arkady on 21.08.2015.
 */

@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public Company save(Company company) {
        return companyRepository.saveAndFlush(company);
    }

    @Override
    public Company findById(Integer id) {
        return companyRepository.findOne(id);
    }

    @Override
    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        companyRepository.delete(id);
    }

    @Override
    public boolean exist(Integer id) {
        return companyRepository.exists(id);
    }

    @Override
    public Company findByName(String name) {
        return companyRepository.findByName(name);
    }
}