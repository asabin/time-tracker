package org.arkady.timetracker.service;

import org.arkady.timetracker.entity.Company;

import java.util.List;

/**
 * Created by Arkady on 21.08.2015.
 */

public interface CompanyService {

    Company save(Company company);

    Company findById(Integer id);

    List<Company> findAll();

    void delete(Integer id);

    boolean exist(Integer id);

    Company findByName(String name);
}