package org.arkady.timetracker.service;

import org.arkady.timetracker.entity.Employee;

import java.util.List;

/**
 * Created by Arkady on 21.08.2015.
 */

public interface EmployeeService {

    Employee save(Employee employee);

    Employee findById(Integer id);

    List<Employee> findAll();

    void delete(Integer id);

    boolean exist(Integer id);

    Employee findByEmail(String email);
}