package org.arkady.timetracker.service;

import org.arkady.timetracker.entity.Task;

import java.util.List;

/**
 * Created by Arkady on 21.08.2015.
 */

public interface TaskService {

    Task save(Task task);

    Task findById(Integer id);

    List<Task> findAll();

    void delete(Integer id);

    boolean exist(Integer id);

    Task findByName(String name);
}