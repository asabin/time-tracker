package org.arkady.timetracker.service;

import org.arkady.timetracker.entity.Project;

import java.util.List;

/**
 * Created by Arkady on 21.08.2015.
 */

public interface ProjectService {

    Project save(Project project);

    Project findById(Integer id);

    List<Project> findAll();

    void delete(Integer id);

    boolean exist(Integer id);

    Project findByName(String name);
}