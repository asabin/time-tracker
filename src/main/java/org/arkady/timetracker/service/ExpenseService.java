package org.arkady.timetracker.service;

import org.arkady.timetracker.entity.Expense;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by Arkady on 22.08.2015.
 */

public interface ExpenseService {

    Expense save(Expense expense);

    Expense findById(int id);

    List<Expense> findAll();

    void delete(int taskId);

    boolean exist(Integer id);

    Expense findByTaskIdAndDate(int taskId, LocalDate date);
}