package org.arkady.timetracker.service;

import org.arkady.timetracker.entity.Company;
import org.arkady.timetracker.entity.Employee;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

/**
 * Created by Arkady on 23.08.2015.
 */

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = TimeTrackerApplication.class)
public class CompanyServiceTest {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private EmployeeService employeeService;

    // 2.
//    @Test
    public void testAll() {
        testSave();
        testFindById();
        testFindAll();
        testDelete();
        testExist();
        testFindByName();
        testAddCompanyWithEmployee();
        testDeleteCompanyWithEmployee();
    }

    // 7.
//    @Test
    // If employee has company and project, and company has this project, => for these company and project
    // it works if for fk_employee_has_company_company and fk_employee_has_project_project ON DELETE = CASCADE action
    public void testAfterProjectTest() {
        testDeleteCompanyWithProject();
    }

    //    @Test
    public void testSave() {
        Company company = new Company();

        company.setName("Company");
        company.setStatus(true);
        company.setCreated(LocalDate.now());

        company = companyService.save(company);
        Assert.assertNotNull(company);
        Assert.assertTrue(companyService.exist(company.getId()));
    }

    //    @Test
    public void testFindById() {
        Assert.assertNotNull(companyService.findById(1));
    }

    //    @Test
    public void testFindAll() {
        Assert.assertNotNull(companyService.findAll());
    }

    //    @Test
    public void testDelete() {
        Company company = new Company();

        company.setName("Company2");
        company.setStatus(true);
        company.setCreated(LocalDate.now());

        company = companyService.save(company);
        Assert.assertNotNull(company);
        Assert.assertTrue(companyService.exist(company.getId()));

        companyService.delete(company.getId());
        Assert.assertFalse(companyService.exist(company.getId()));
    }

    //    @Test
    public void testExist() {
        Assert.assertTrue(companyService.exist(1));
    }

    //    @Test
    public void testFindByName() {
        Assert.assertNotNull(companyService.findByName("Company"));
    }

    //    @Test
    public void testDeleteCompanyWithProject() {
        companyService.delete(1);
        Assert.assertFalse(companyService.exist(1));
    }

    //    @Test
    public void testAddCompanyWithEmployee() {
        Company company = new Company();

        company.setName("Company3");
        company.setStatus(true);
        company.setCreated(LocalDate.now());

        Employee employee = employeeService.findById(1);
        Assert.assertNotNull(employee);
        company.getEmployees().add(employee);

        company = companyService.save(company);
        Assert.assertNotNull(company);
        Assert.assertTrue(companyService.exist(company.getId()));
        Assert.assertNotNull(company.getEmployees());
    }

    //    @Test
    public void testDeleteCompanyWithEmployee() {
        companyService.delete(3);
        Assert.assertFalse(companyService.exist(3));
        Assert.assertTrue(employeeService.exist(1));
    }
}