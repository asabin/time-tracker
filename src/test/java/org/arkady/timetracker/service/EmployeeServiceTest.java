package org.arkady.timetracker.service;

import org.arkady.timetracker.entity.Company;
import org.arkady.timetracker.entity.Employee;
import org.arkady.timetracker.entity.Project;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

/**
 * Created by Arkady on 24.08.2015.
 */

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = TimeTrackerApplication.class)
public class EmployeeServiceTest {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ProjectService projectService;

    // 1.
//    @Test
    public void testAll() {
        testSave();
        testFindById();
        testFindAll();
        testDelete();
        testExist();
        testFindByEmail();
    }

    // 6.
//    @Test
    public void testAfterTestCompanyAndProject() {
        testAddCompanyAndProjectToEmployee();
        testAddEmployeeWithCompanyAndProject();
        testDeleteEmployeeWithCompanyAndProject();
        testDeleteEmployeeWithCompanyAndProjectAndTaskAndExpensive();
    }

    //    @Test
    public void testSave() {
        Employee employee = new Employee();

        employee.setName("Employee");
        employee.setSurname("Employee");
        employee.setEmail("employee@email");
        employee.setPassword("emp");
        employee.setRole(0);
        employee.setCreated(LocalDate.now());

        Assert.assertNotNull(employeeService.save(employee));
        Assert.assertTrue(employeeService.exist(employee.getId()));
    }

    //    @Test
    public void testFindById() {
        Assert.assertNotNull(employeeService.findById(1));
    }

    //    @Test
    public void testFindAll() {
        Assert.assertNotNull(employeeService.findAll());
    }

    //    @Test
    public void testDelete() {
        Employee employee = new Employee();

        employee.setName("Employee2");
        employee.setSurname("Employee2");
        employee.setEmail("employee2@email");
        employee.setPassword("emp2");
        employee.setRole(0);
        employee.setCreated(LocalDate.now());

        employee = employeeService.save(employee);
        Assert.assertNotNull(employee);
        Assert.assertTrue(employeeService.exist(employee.getId()));

        employeeService.delete(employee.getId());
        Assert.assertFalse(employeeService.exist(employee.getId()));
    }

    //    @Test
    public void testExist() {
        Assert.assertTrue(employeeService.exist(1));
    }

    //    @Test
    public void testFindByEmail() {
        Assert.assertNotNull(employeeService.findByEmail("employee@email"));
    }

    //    @Test
    public void testAddCompanyAndProjectToEmployee() {
        Employee employee = employeeService.findById(1);
        Assert.assertNotNull(employee);

        Company company = companyService.findById(1);
        Assert.assertNotNull(company);
        employee.getCompanies().add(company);

        Project project = projectService.findById(1);
        Assert.assertNotNull(project);
        employee.getProjects().add(project);

        employee = employeeService.save(employee);
        Assert.assertNotNull(employee);
        Assert.assertFalse(employee.getCompanies().isEmpty());
        Assert.assertFalse(employee.getProjects().isEmpty());
    }

    //    @Test
    public void testAddEmployeeWithCompanyAndProject() {
        Employee employee = new Employee();

        employee.setName("Employee3");
        employee.setSurname("Employee3");
        employee.setEmail("employee3@email");
        employee.setPassword("emp3");
        employee.setRole(0);
        employee.setCreated(LocalDate.now());

        Company company = companyService.findById(1);
        Assert.assertNotNull(company);
        employee.getCompanies().add(company);

        Project project = projectService.findById(1);
        Assert.assertNotNull(project);
        employee.getProjects().add(project);

        employee = employeeService.save(employee);
        Assert.assertNotNull(employee);
        Assert.assertTrue(employeeService.exist(employee.getId()));
        Assert.assertFalse(employee.getCompanies().isEmpty());
        Assert.assertFalse(employee.getProjects().isEmpty());
    }

    //    @Test
    public void testDeleteEmployeeWithCompanyAndProject() {
        employeeService.delete(3);
        Assert.assertFalse(employeeService.exist(3));

        Company company = companyService.findById(1);
        Assert.assertNotNull(company);
        Assert.assertTrue(company.getEmployees().size() == 1);

        Project project = projectService.findById(1);
        Assert.assertNotNull(project);
        Assert.assertTrue(project.getEmployees().size() == 1);
    }

    //    @Test
    public void testDeleteEmployeeWithCompanyAndProjectAndTaskAndExpensive() {
        employeeService.delete(1);
        Assert.assertFalse(employeeService.exist(1));

        Company company = companyService.findById(1);
        Assert.assertNotNull(company);
        Assert.assertTrue(company.getEmployees().isEmpty());

        Project project = projectService.findById(1);
        Assert.assertNotNull(project);
        Assert.assertTrue(project.getEmployees().isEmpty());
    }
}