package org.arkady.timetracker.service;

import org.arkady.timetracker.entity.Expense;
import org.arkady.timetracker.entity.Task;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

/**
 * Created by Arkady on 26.08.2015.
 */

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = TimeTrackerApplication.class)
public class ExpenseServiceTest {

    @Autowired
    private ExpenseService expenseService;

    @Autowired
    private TaskService taskService;

    // 5.
//    @Test
    public void testAll() {
        testSave();
        testFindById();
        testFindAll();
        testDelete();
        testExist();
        testFindByTaskIdAndDate();
    }

    //    @Test
    public void testSave() {
        Expense expense = new Expense();

        expense.setDate(LocalDate.now());
        expense.setAmount(8);

        Task task = taskService.findById(1);
        expense.setTask(task);

        expense = expenseService.save(expense);
        Assert.assertNotNull(expense);
        Assert.assertTrue(expenseService.exist(expense.getId()));
    }

    //    @Test
    public void testFindById() {
        Assert.assertNotNull(expenseService.findById(1));
    }

    //    @Test
    public void testFindAll() {
        Assert.assertNotNull(expenseService.findAll());
    }

    //    @Test
    public void testDelete() {
        Expense expense = new Expense();

        expense.setDate(LocalDate.now().minusDays(1));
        expense.setAmount(8);

        Task task = taskService.findById(1);
        expense.setTask(task);

        expense = expenseService.save(expense);
        Assert.assertNotNull(expense);

        expenseService.delete(expense.getId());
        Assert.assertNull(expenseService.findById(expense.getId()));
    }

    //    @Test
    public void testExist() {
        Assert.assertTrue(expenseService.exist(1));
    }

    //    @Test
    public void testFindByTaskIdAndDate() {
        Assert.assertNotNull(expenseService.findByTaskIdAndDate(1, LocalDate.now()));
    }
}