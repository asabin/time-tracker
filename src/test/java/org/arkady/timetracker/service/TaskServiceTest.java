package org.arkady.timetracker.service;

import org.arkady.timetracker.entity.Employee;
import org.arkady.timetracker.entity.Project;
import org.arkady.timetracker.entity.Task;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

/**
 * Created by Arkady on 25.08.2015.
 */

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = TimeTrackerApplication.class)
public class TaskServiceTest {

    @Autowired
    private TaskService taskService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private ProjectService projectService;

    // 4.
//    @Test
    public void testAll() {
        testSave();
        testFindById();
        testFindAll();
        testDelete();
        testExist();
        testFindByName();
    }

    //    @Test
    public void testSave() {
        Task task = new Task();

        task.setName("Task");
        task.setStatus(true);
        task.setOpened(LocalDate.now());

        Employee employee = employeeService.findById(1);
        Assert.assertNotNull(employee);
        task.setEmployee(employee);

        Project project = projectService.findById(1);
        Assert.assertNotNull(project);
        task.setProject(project);

        task = taskService.save(task);
        Assert.assertNotNull(task);
        Assert.assertTrue(taskService.exist(task.getId()));
    }

    //    @Test
    public void testFindById() {
        Assert.assertNotNull(taskService.findById(1));
    }

    //    @Test
    public void testFindAll() {
        Assert.assertNotNull(taskService.findAll());
    }

    //    @Test
    public void testDelete() {
        Task task = new Task();

        task.setName("Task2");
        task.setStatus(true);
        task.setOpened(LocalDate.now());
        task.setClosed(LocalDate.now());

        Employee employee = employeeService.findById(1);
        Assert.assertNotNull(employee);
        task.setEmployee(employee);

        Project project = projectService.findById(1);
        Assert.assertNotNull(project);
        task.setProject(project);

        task = taskService.save(task);
        Assert.assertNotNull(task);
        Assert.assertTrue(taskService.exist(task.getId()));

        taskService.delete(task.getId());
        Assert.assertFalse(taskService.exist(task.getId()));
    }

    //    @Test
    public void testExist() {
        Assert.assertTrue(taskService.exist(1));
    }

    //    @Test
    public void testFindByName() {
        Assert.assertNotNull(taskService.findByName("Task"));
    }
}