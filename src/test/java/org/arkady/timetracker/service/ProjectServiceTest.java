package org.arkady.timetracker.service;

import org.arkady.timetracker.entity.Company;
import org.arkady.timetracker.entity.Project;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

/**
 * Created by Arkady on 25.08.2015.
 */

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = TimeTrackerApplication.class)
public class ProjectServiceTest {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private CompanyService companyService;

    // 3.
//    @Test
    public void testAll() {
        testSave();
        testFindById();
        testFindAll();
        testDelete();
        testExist();
        testFindByName();
    }

    //    @Test
    public void testSave() {
        Project project = new Project();

        project.setName("Project");
        project.setStatus(true);
        project.setStarted(LocalDate.now());

        Company company = companyService.findById(1);
        Assert.assertNotNull(company);
        project.setCompany(company);

        project = projectService.save(project);
        Assert.assertNotNull(project);
        Assert.assertTrue(projectService.exist(project.getId()));
        Assert.assertNotNull(project.getCompany());
    }

    //    @Test
    public void testFindById() {
        Assert.assertNotNull(projectService.findById(1));
    }

    //    @Test
    public void testFindAll() {
        Assert.assertNotNull(projectService.findAll());
    }

    //    @Test
    public void testDelete() {
        Project project = new Project();

        project.setName("Project2");
        project.setStatus(true);
        project.setStarted(LocalDate.now());
        project.setFinished(LocalDate.now());

        Company company = companyService.findById(1);
        Assert.assertNotNull(company);
        project.setCompany(company);

        project = projectService.save(project);
        Assert.assertNotNull(project);
        Assert.assertTrue(projectService.exist(project.getId()));

        projectService.delete(project.getId());
        Assert.assertFalse(projectService.exist(project.getId()));
    }

    //    @Test
    public void testExist() {
        Assert.assertTrue(projectService.exist(1));
    }

    //    @Test
    public void testFindByName() {
        Assert.assertNotNull(projectService.findByName("Project"));
    }
}